package com.kpn.topclosestrestaurants.service;

import com.kpn.topclosestrestaurants.Application;
import com.kpn.topclosestrestaurants.model.Query;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.service.adapter.SearchApiAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class RestaurantsServiceImplTest {

    @Mock
    private SearchApiAdapter adapter;

    @Mock
    private Query query;

    private RestaurantsServiceImpl restaurantsServiceImplTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        restaurantsServiceImplTest = new RestaurantsServiceImpl(adapter);
    }

    @Test
    public void testGetRestaurants() {
        RecommendedVenues recommendedVenuesResponse = new RecommendedVenues();

        when(adapter.makeQuery()).thenReturn(recommendedVenuesResponse);

        RecommendedVenues recommendedVenuesTest = restaurantsServiceImplTest.getRestaurants(query);

        assertNotNull(recommendedVenuesTest);
        assertEquals(recommendedVenuesResponse, recommendedVenuesTest);
    }
}
