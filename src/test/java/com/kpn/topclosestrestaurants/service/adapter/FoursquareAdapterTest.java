package com.kpn.topclosestrestaurants.service.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kpn.topclosestrestaurants.Application;
import com.kpn.topclosestrestaurants.model.FoursquareQuery;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.model.Result;
import com.kpn.topclosestrestaurants.model.SearchRecommendationsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class FoursquareAdapterTest {

    @Mock
    private RestTemplate restTemplate;

    private FoursquareAdapter adapter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        adapter = new FoursquareAdapter(restTemplate);
    }

    @Test
    public void testMakeQuery() throws IOException {
        final String query = "food";
        final String latLon = "test_latLon";
        final Integer page = 0;
        final Integer priceFilter = 0;
        final Integer featureFilter = 0;

        FoursquareQuery.FoursquareQueryBuilder foursquareQueryBuilder = new FoursquareQuery.FoursquareQueryBuilder();

        FoursquareQuery foursquareQuery = foursquareQueryBuilder.setQuery(query)
                .setLatLon(latLon)
                .setPage(page)
                .setPriceFilter(priceFilter)
                .setFeatureFilter(featureFilter)
                .build();

        adapter.setQuery(foursquareQuery);

        String endpoint = adapter.buildEndpoint();

        ObjectMapper objectMapper = new ObjectMapper();
        SearchRecommendationsResponse response = objectMapper.readValue(TestUtil.response, SearchRecommendationsResponse.class);
        when(restTemplate.getForObject(endpoint, SearchRecommendationsResponse.class)).thenReturn(response);

        RecommendedVenues recommendedVenuesTest = adapter.makeQuery();

        assertNotNull(recommendedVenuesTest);

        Result result = recommendedVenuesTest.getResults().get(0);

        assertEquals(result.getVenue().getName(), "Test_Venue");
    }
}
