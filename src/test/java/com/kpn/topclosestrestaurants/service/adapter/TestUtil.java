package com.kpn.topclosestrestaurants.service.adapter;

class TestUtil {

    static final String response = "{\n" +
            "    \"meta\": {\n" +
            "        \"code\": 200,\n" +
            "        \"requestId\": \"5b0193414c1f673dc478819e\"\n" +
            "    },\n" +
            "    \"response\": {\n" +
            "        \"normalizedQuery\": \"food\",\n" +
            "        \"group\": {\n" +
            "            \"results\": [\n" +
            "                {\n" +
            "                    \"displayType\": \"venue\",\n" +
            "                    \"venue\": {\n" +
            "                        \"id\": \"5809da9a38fab6b0dee9c796\",\n" +
            "                        \"name\": \"Test_Venue\",\n" +
            "                        \"location\": {\n" +
            "                            \"address\": \"Watergarden İstanbul\",\n" +
            "                            \"crossStreet\": \"Barbaros Mah. Kızıl Begonya Sok. No:10/1\",\n" +
            "                            \"lat\": 40.997845,\n" +
            "                            \"lng\": 29.099852,\n" +
            "                            \"labeledLatLngs\": [\n" +
            "                                {\n" +
            "                                    \"label\": \"display\",\n" +
            "                                    \"lat\": 40.997845,\n" +
            "                                    \"lng\": 29.099852\n" +
            "                                }\n" +
            "                            ],\n" +
            "                            \"distance\": 61,\n" +
            "                            \"postalCode\": \"34746\",\n" +
            "                            \"cc\": \"TR\",\n" +
            "                            \"neighborhood\": \"Barbaros\",\n" +
            "                            \"city\": \"Ataşehir\",\n" +
            "                            \"state\": \"İstanbul\",\n" +
            "                            \"country\": \"Türkiye\",\n" +
            "                            \"formattedAddress\": [\n" +
            "                                \"Watergarden İstanbul (Barbaros Mah. Kızıl Begonya Sok. No:10/1)\",\n" +
            "                                \"34746 Ataşehir\",\n" +
            "                                \"Türkiye\"\n" +
            "                            ]\n" +
            "                        },\n" +
            "                        \"categories\": [\n" +
            "                            {\n" +
            "                                \"id\": \"4bf58dd8d48988d1c4941735\",\n" +
            "                                \"name\": \"Restaurant\",\n" +
            "                                \"pluralName\": \"Restaurants\",\n" +
            "                                \"shortName\": \"Restaurant\",\n" +
            "                                \"icon\": {\n" +
            "                                    \"prefix\": \"https://ss3.4sqi.net/img/categories_v2/food/default_\",\n" +
            "                                    \"suffix\": \".png\"\n" +
            "                                },\n" +
            "                                \"primary\": true\n" +
            "                            }\n" +
            "                        ],\n" +
            "                        \"dislike\": false,\n" +
            "                        \"ok\": false\n" +
            "                    },\n" +
            "                    \"id\": \"5b019342c5b11c42e395e706\",\n" +
            "                    \"photo\": {\n" +
            "                        \"id\": \"5813208e38fa6a3a66a00c73\",\n" +
            "                        \"createdAt\": 1477648526,\n" +
            "                        \"prefix\": \"https://igx.4sqi.net/img/general/\",\n" +
            "                        \"suffix\": \"/52928698_5pjc1-OfbWxsZkvcIZBjYS4r-c1Jy2k48NEuHqkyLiA.jpg\",\n" +
            "                        \"width\": 1787,\n" +
            "                        \"height\": 1440,\n" +
            "                        \"visibility\": \"public\"\n" +
            "                    },\n" +
            "                    \"snippets\": {\n" +
            "                        \"count\": 1,\n" +
            "                        \"items\": [\n" +
            "                            {\n" +
            "                                \"detail\": {\n" +
            "                                    \"type\": \"tip\",\n" +
            "                                    \"object\": {\n" +
            "                                        \"id\": \"5a09db93135b392214a8552c\",\n" +
            "                                        \"createdAt\": 1510595475,\n" +
            "                                        \"text\": \"Variety of great food, great ambience , great service .\",\n" +
            "                                        \"type\": \"user\",\n" +
            "                                        \"canonicalUrl\": \"https://foursquare.com/item/5a09db93135b392214a8552c\",\n" +
            "                                        \"logView\": true,\n" +
            "                                        \"agreeCount\": 0,\n" +
            "                                        \"disagreeCount\": 0,\n" +
            "                                        \"todo\": {\n" +
            "                                            \"count\": 0\n" +
            "                                        },\n" +
            "                                        \"user\": {\n" +
            "                                            \"id\": \"122230555\",\n" +
            "                                            \"firstName\": \"Fayoota\",\n" +
            "                                            \"lastName\": \"Ab\",\n" +
            "                                            \"gender\": \"female\",\n" +
            "                                            \"photo\": {\n" +
            "                                                \"prefix\": \"https://igx.4sqi.net/img/user/\",\n" +
            "                                                \"suffix\": \"/122230555-VIZLWTVRPDLWPLQU.jpg\"\n" +
            "                                            }\n" +
            "                                        }\n" +
            "                                    }\n" +
            "                                }\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                }\n" +
            "            ],\n" +
            "            \"totalResults\": 122\n" +
            "        },\n" +
            "        \"context\": {\n" +
            "            \"searchLocationNearYou\": true,\n" +
            "            \"searchLocationMapBounds\": false,\n" +
            "            \"searchLocationType\": \"NearYou\",\n" +
            "            \"currentLocation\": {\n" +
            "                \"what\": \"\",\n" +
            "                \"where\": \"\",\n" +
            "                \"feature\": {\n" +
            "                    \"cc\": \"TR\",\n" +
            "                    \"name\": \"Barbaros - Atatürk\",\n" +
            "                    \"displayName\": \"Barbaros - Atatürk, Istanbul\",\n" +
            "                    \"woeType\": 22,\n" +
            "                    \"slug\": \"barbaros---ataturk-turkey\",\n" +
            "                    \"id\": \"maponics:10001196\",\n" +
            "                    \"longId\": \"10001196\",\n" +
            "                    \"geometry\": {\n" +
            "                        \"center\": {\n" +
            "                            \"lat\": 40.991853,\n" +
            "                            \"lng\": 29.114306\n" +
            "                        },\n" +
            "                        \"bounds\": {\n" +
            "                            \"ne\": {\n" +
            "                                \"lat\": 41.00357,\n" +
            "                                \"lng\": 29.146616\n" +
            "                            },\n" +
            "                            \"sw\": {\n" +
            "                                \"lat\": 40.982361,\n" +
            "                                \"lng\": 29.090026\n" +
            "                            }\n" +
            "                        }\n" +
            "                    },\n" +
            "                    \"encumbered\": true\n" +
            "                },\n" +
            "                \"parents\": []\n" +
            "            },\n" +
            "            \"boundsSummaryRadius\": 816,\n" +
            "            \"relatedNeighborhoods\": [],\n" +
            "            \"geoParams\": {\n" +
            "                \"ll\": \"40.998336699999996,29.099529299999997\",\n" +
            "                \"radius\": \"816\"\n" +
            "            },\n" +
            "            \"geoBounds\": {\n" +
            "                \"circle\": {\n" +
            "                    \"center\": {\n" +
            "                        \"lat\": 40.998336699999996,\n" +
            "                        \"lng\": 29.099529299999997\n" +
            "                    },\n" +
            "                    \"radius\": 816\n" +
            "                }\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "}";
}
