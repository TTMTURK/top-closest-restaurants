package com.kpn.topclosestrestaurants.controller;

import com.kpn.topclosestrestaurants.Application;
import com.kpn.topclosestrestaurants.model.FoursquareQuery;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.service.RestaurantsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class RestaurantsControllerTest {

    @Mock
    private RestaurantsService restaurantsService;

    @Mock
    private FoursquareQuery.FoursquareQueryBuilder foursquareQueryBuilder;

    @Mock
    private FoursquareQuery foursquareQuery;

    private RestaurantsController restaurantsController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        restaurantsController = new RestaurantsController(foursquareQueryBuilder, restaurantsService);
    }

    @Test
    public void testGetRestaurants() {
        final String query = "food";
        final String latLon = "test_latLon";
        final Integer page = 0;
        final Integer priceFilter = 0;
        final Integer featureFilter = 0;

        when(foursquareQueryBuilder.setQuery(query)).thenReturn(foursquareQueryBuilder);
        when(foursquareQueryBuilder.setLatLon(latLon)).thenReturn(foursquareQueryBuilder);
        when(foursquareQueryBuilder.setPage(page)).thenReturn(foursquareQueryBuilder);
        when(foursquareQueryBuilder.setPriceFilter(priceFilter)).thenReturn(foursquareQueryBuilder);
        when(foursquareQueryBuilder.setFeatureFilter(featureFilter)).thenReturn(foursquareQueryBuilder);
        when(foursquareQueryBuilder.build()).thenReturn(foursquareQuery);

        RecommendedVenues recommendedVenuesResponse = new RecommendedVenues();

        when(restaurantsService.getRestaurants(foursquareQuery)).thenReturn(recommendedVenuesResponse);

        RecommendedVenues recommendedVenuesTest = restaurantsController.getRestaurants(latLon, page, priceFilter, featureFilter);

        assertNotNull(recommendedVenuesTest);
        assertEquals(recommendedVenuesResponse, recommendedVenuesTest);
    }
}