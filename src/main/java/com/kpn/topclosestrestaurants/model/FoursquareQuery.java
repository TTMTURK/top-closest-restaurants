package com.kpn.topclosestrestaurants.model;

import lombok.Getter;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Getter
@ToString
public class FoursquareQuery implements Query {

    private final String query;
    private final String latLon;
    private final Integer page;
    private final Integer priceFilter;
    private final Integer featureFilter;

    private FoursquareQuery(FoursquareQueryBuilder builder) {
        this.query = builder.query;
        this.latLon = builder.latLon;
        this.page = builder.page;
        this.priceFilter = builder.priceFilter;
        this.featureFilter = builder.featureFilter;
    }

    @Component
    public static class FoursquareQueryBuilder {

        private String query;
        private String latLon;
        private Integer page;
        private Integer priceFilter;
        private Integer featureFilter;

        public FoursquareQueryBuilder setQuery(String query) {
            this.query = query;
            return this;
        }

        public FoursquareQueryBuilder setLatLon(String latLon) {
            this.latLon = latLon;
            return this;
        }

        public FoursquareQueryBuilder setPage(Integer page) {
            this.page = page;
            return this;
        }

        public FoursquareQueryBuilder setPriceFilter(Integer priceFilter) {
            this.priceFilter = priceFilter;
            return this;
        }

        public FoursquareQueryBuilder setFeatureFilter(Integer featureFilter) {
            this.featureFilter = featureFilter;
            return this;
        }

        public FoursquareQuery build() {
            return new FoursquareQuery(this);
        }

    }


}
