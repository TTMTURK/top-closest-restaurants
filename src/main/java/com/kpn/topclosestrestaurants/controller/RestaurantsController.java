package com.kpn.topclosestrestaurants.controller;

import com.kpn.topclosestrestaurants.model.FoursquareQuery;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.service.RestaurantsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@EnableCaching
@RequestMapping("/api/foursquare")
public class RestaurantsController {

    private static Logger LOGGER = LoggerFactory.getLogger(RestaurantsController.class);

    private final FoursquareQuery.FoursquareQueryBuilder foursquareQueryBuilder;

    private final RestaurantsService restaurantsService;

    @Autowired
    public RestaurantsController(FoursquareQuery.FoursquareQueryBuilder foursquareQueryBuilder, RestaurantsService restaurantsService) {
        this.foursquareQueryBuilder = foursquareQueryBuilder;
        this.restaurantsService = restaurantsService;
    }

    @Cacheable(value = "restaurants")
    @GetMapping(value = "/restaurants", produces = APPLICATION_JSON_VALUE)
    public RecommendedVenues getRestaurants(@RequestParam(value = "latitudeLongitude") final String latLon,
                                            @RequestParam(value = "page", defaultValue = "0") final Integer page,
                                            @RequestParam(value = "priceFilter", required = false) final Integer priceFilter,
                                            @RequestParam(value = "featureFilter", required = false) final Integer featureFilter) {
        final String QUERY = "food";

        FoursquareQuery foursquareQuery = foursquareQueryBuilder.setQuery(QUERY)
                .setLatLon(latLon)
                .setPage(page)
                .setPriceFilter(priceFilter)
                .setFeatureFilter(featureFilter)
                .build();

        LOGGER.info("Getting restaurants " + foursquareQuery);

        return restaurantsService.getRestaurants(foursquareQuery);
    }

    @CacheEvict(value = "restaurants", allEntries = true)
    @GetMapping(value = "/resetRestaurants")
    public void resetRestaurants() {
        LOGGER.info("Clearing cached restaurants");
    }
}
