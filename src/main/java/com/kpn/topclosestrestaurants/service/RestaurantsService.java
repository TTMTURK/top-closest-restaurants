package com.kpn.topclosestrestaurants.service;

import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.model.Query;

public interface RestaurantsService {

    RecommendedVenues getRestaurants(Query query);

}
