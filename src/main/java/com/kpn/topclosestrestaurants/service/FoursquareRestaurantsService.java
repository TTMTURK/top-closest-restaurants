package com.kpn.topclosestrestaurants.service;

import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.model.Query;
import com.kpn.topclosestrestaurants.service.adapter.SearchApiAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class FoursquareRestaurantsService implements RestaurantsService {

    private final RestaurantsServiceImpl restaurantsService;

    @Autowired
    public FoursquareRestaurantsService(SearchApiAdapter foursquareAdapter) {
        this.restaurantsService = new RestaurantsServiceImpl(foursquareAdapter);
    }

    @Override
    public RecommendedVenues getRestaurants(Query query) {

        return restaurantsService.getRestaurants(query);
    }
}
