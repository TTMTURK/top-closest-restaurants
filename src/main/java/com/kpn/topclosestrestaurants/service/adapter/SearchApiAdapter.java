package com.kpn.topclosestrestaurants.service.adapter;

import com.kpn.topclosestrestaurants.model.Query;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;

public interface SearchApiAdapter {

    void setQuery(Query query);

    RecommendedVenues makeQuery();

}
