package com.kpn.topclosestrestaurants.service.adapter;

import com.kpn.topclosestrestaurants.model.FoursquareQuery;
import com.kpn.topclosestrestaurants.model.Query;
import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.model.SearchRecommendationsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@PropertySource("classpath:foursquare_adapter.properties")
public class FoursquareAdapter implements SearchApiAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(FoursquareAdapter.class);

    @Value("${search_recommendations_endpoint}")
    private String FOURSQUARE_SEARCH_RECOMMENDATIONS_ENDPOINT;
    @Value("${client_id}")
    private String CLIENT_ID;
    @Value("${client_secret}")
    private String CLIENT_SECRET;
    @Value("${version}")
    private String VERSION;
    @Value("${data_per_page}")
    private int DATA_PER_PAGE;

    private Query query;

    private final RestTemplate restTemplate;

    @Autowired
    public FoursquareAdapter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    /**
     * Return object including list of venues and totalResults
     *
     * @return RecommendedVenues
     */
    public RecommendedVenues makeQuery() {
        String foursquareEndpoint = buildEndpoint();

        LOGGER.debug("foursquareEndpoint: " + foursquareEndpoint);

        SearchRecommendationsResponse searchRecommendationsResponse = restTemplate.getForObject(foursquareEndpoint, SearchRecommendationsResponse.class);

        return getRecommendedVenues(searchRecommendationsResponse);
    }

    /**
     * Return endpoint to send GET req
     *
     * @return String
     */
    String buildEndpoint() {
        FoursquareQuery foursquareQuery = (FoursquareQuery) query;

        Map<String, String> params = new HashMap<>();
        params.put("client_id", CLIENT_ID);
        params.put("client_secret", CLIENT_SECRET);
        params.put("v", VERSION);
        params.put("query", foursquareQuery.getQuery());
        params.put("limit", Integer.toString(DATA_PER_PAGE));
        params.put("offset", Integer.toString(DATA_PER_PAGE * foursquareQuery.getPage()));
        params.put("ll", foursquareQuery.getLatLon());


        if (foursquareQuery.getPriceFilter() != null) {
            params.put("prices", Integer.toString(foursquareQuery.getPriceFilter()));
        }

        if (foursquareQuery.getFeatureFilter() != null) {
            params.put("features", Integer.toString(foursquareQuery.getFeatureFilter()));
        }

        String paramsStr = params.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));

        return FOURSQUARE_SEARCH_RECOMMENDATIONS_ENDPOINT + "?" + paramsStr;
    }

    private RecommendedVenues getRecommendedVenues(SearchRecommendationsResponse searchRecommendationsResponse) {

        return searchRecommendationsResponse.getResponse().getGroup();
    }
}
