package com.kpn.topclosestrestaurants.service;

import com.kpn.topclosestrestaurants.model.RecommendedVenues;
import com.kpn.topclosestrestaurants.model.Query;
import com.kpn.topclosestrestaurants.service.adapter.SearchApiAdapter;

public class RestaurantsServiceImpl implements RestaurantsService {

    private final SearchApiAdapter searchApiAdapter;

    RestaurantsServiceImpl(SearchApiAdapter adapter) {
        this.searchApiAdapter = adapter;
    }

    @Override
    public RecommendedVenues getRestaurants(Query query) {
        this.searchApiAdapter.setQuery(query);

        return searchApiAdapter.makeQuery();
    }

}
