var idleTime = 0;
var latLon;

$(document).ready(function () {

    setInterval(incrementTimer, 1000); // Every second

    $(this).mousedown(function (e) {
        idleTime = 0;
    });
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });

    getLocation();

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, errPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        latLon = position.coords.latitude + ',' + position.coords.longitude;
        console.info('Current location: '+latLon);

        getRestaurants(latLon);
    }
    
    function errPosition(err) {
        latLon = "52.3725314,4.8929215";
        console.info("Could not get current location, use predefined location: " + latLon);

        getRestaurants(latLon);
    }

    function getRestaurants(latLon, page) {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/api/foursquare/restaurants",
            data: {
                "latitudeLongitude": latLon,
                "page": page
            },
            success: function (res) {
                parseRes(res);
            },
            error: function (e) {
                alert("ERROR: ", e);
                console.error("ERROR: ", e);
            }
        });
        $(".dropdown-toggle").dropdown();

        document.getElementById("priceFilter").onchange = function (ev) {
            filterFunction()
        };
        document.getElementById("featureFilter").onchange = function (ev) {
            filterFunction()
        };
    }

    function getRestaurantsWithFilter(latLon, page, priceFilter, featureFilter) {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/api/foursquare/restaurants",
            data: {
                "latitudeLongitude": latLon,
                "page": page,
                "priceFilter": priceFilter,
                "featureFilter": featureFilter
            },
            success: function (res) {
                parseRes(res);
            },
            error: function (e) {
                alert("ERROR: ", e);
                console.error("ERROR: ", e);
            }
        });
        $(".dropdown-toggle").dropdown();
    }

    function parseRes(res) {
        $('#restaurantsTable tbody').empty();
        $('#pagin').empty();

        $.each(res.results, function (i, results) {
            var customerRow = '<tr>' +
                '<td>' + results.venue.name + '</td>' +
                '</tr>';

            $('#restaurantsTable tbody').append(customerRow);
        });

        $("#restaurantsTable tbody tr:odd").addClass("info");
        $("#restaurantsTable tbody tr:even").addClass("success");

        var pageAligned;
        if (res.totalResults <= 10) {
            pageAligned = 0;
        } else {
            var pageCount = res.totalResults / 10;
            pageAligned = pageCount > 0 ? (pageCount % 10 == 0 ? pageCount - 1 : pageCount) : 0;
        }

        for (var i = 0; i < pageAligned; i++) {
            if (i == 0)
                $("#pagin").append('<li><a class="current" href="#">' + (i + 1) + '</a></li>');
            else
                $("#pagin").append('<li><a href="#">' + (i + 1) + '</a></li>');
        }

        $("#pagin li a").click(function () {
            $("#pagin li a").removeClass("current");
            $(this).addClass("current");
            showPage(parseInt($(this).text()))
        });

    }

    function filterFunction() {
        var priceFilter = document.getElementById("priceFilter").value;
        var featureFilter = document.getElementById("featureFilter").value;

        getRestaurantsWithFilter(latLon, 0, priceFilter, featureFilter);
    }

    showPage = function (page) {
        var priceFilter = document.getElementById("priceFilter").value;
        var featureFilter = document.getElementById("featureFilter").value;
        
        if (priceFilter == "" && featureFilter == "") {
            getRestaurants(latLon, page-1);
        } else {
            getRestaurantsWithFilter(latLon, page-1, priceFilter, featureFilter);
        }
    }
});

function incrementTimer() {
    idleTime = idleTime + 1;
    if (idleTime > 60) { // 1 minute
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/api/foursquare/resetRestaurants"
        });
        window.location.reload();
    }
}
