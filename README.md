# KPN coding challenge

## About the project

First of all thank you for considering working with KPN. We admire your efforts and respect your time, therefore we ask you to please carefully read the project description and only work on stuff that is listed among the objectives. 
The overall goal of the assignment is to create a minimal implementation of a restaurant locator based on the Foursquare API.

## Workflow and terminology

- Create a service that calls Foursquare API to get a list of all restaurants around the user of your application.
- Display the list of restaurants paginated and create the possibility to filter it by 2 criterias of your choice. (E.g. top rated by other users or type of cuisine)
- The user session should expire after 1 minute of inactivity on the page.
- Once the session expires the criteria is removed and the list refreshed to original state.

## Goals
### What we would ideally like to see
- Create a service that calls Foursquare API.
- Display the list of restaurants unsorted and unfiltered in a table.
- Create a drop-down menu with 2 filtering criterias of your choice (E.g. top rated by other users or type of cuisine).
- Once a criteria is chosen display the filtered restaurants list in a table.
- The browser session should expire after 1 minute of inactivity on the page.
- Once the browser session expires the criteria is removed and the list refreshed to original state.
- Very basic layout and styling.
- Commit often and avoid rebasing your commits.

## Technology stack

Although we use Spring Framework we put no restrictions on the languages, frameworks, tools and libraries you wish to use so long they support the purpose.

## Time and Deadline

Please do not spend more than 8 hours of development on the project. If you are not completely done it is not a problem, quality over quantity is the motto.

## Disclaimer

This programming challenge is one step in a multiple step hiring process. It is a means to assess the competency of our candidates.
The work you will do - including writing code and/or designing software - is NOT going to be reused, copied or in any form replicated by us or any of our third parties.
If you have any concerns - related to what is stated above or the programming challenge itself - please email me at andrei dot visan at kpn dot com.


*We wish you good luck with the assignment - The team at KPN*

